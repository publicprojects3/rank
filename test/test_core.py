from random import randrange
from unittest.case import TestCase
from app import app as my_app
from rank.models.utils import Util_sql

n: int = randrange(100)
token: str = ''
user: str = "Lucas"
password: str = "abcdef"
error_user: str = user + "error"
error_password: str = password + "error"

class TestNames(TestCase):

    def setUp(self):
        self.response = my_app.test_client().get('/')

    def test_get_home(self):
        self.assertEqual(200, self.response.status_code)

    def test_content_type(self):
        self.assertIn('application/json', self.response.content_type)
        
    def test_response(self):
        alunos = Util_sql()
        names = alunos.select_names
        self.assertEqual(names, self.response.json['names'])

    
class TestRank(TestCase):

    def setUp(self):
        self.response = my_app.test_client().get('/rank')

    def test_get(self):
        self.assertEqual(200, self.response.status_code)

    def test_content_type(self):
        self.assertIn('application/json', self.response.content_type)
        
    def test_response(self):
        Rank = Util_sql().rank_names
        names = [
            {
                "name": name,
                "color": Rank[name][0],
                "score": Rank[name][1]
            }

            for name in Rank
        ]
        self.assertEqual('rank', self.response.json['status'])
        self.assertEqual(names, self.response.json['rank'])


class TestLogin(TestCase):

    def setUp(self):
        self.n: int = n
        self.token: str = token
        self.user: str = user
        self.password: str = password

    def aux(self):
        self.response_login = my_app.test_client().post('/login', json={
            'user': "Lucas",
            'password': "abcdef",
            'exp': 10
        })
        self.token = self.response_login.json['token']

    def test_post(self):
        self.aux()
        self.assertEqual(200, self.response_login.status_code)

    def test_content_type(self):
        self.aux()
        self.assertIn('application/json', self.response_login.content_type)
        
    def test_response_login_token(self):
        self.aux()
        self.assertEqual(269, len(self.response_login.json['token']))

class TestErrorLogin(TestCase):

    def setUp(self):
        self.n: int = n
        self.token: str = token
        self.user: str = error_user
        self.password: str = error_password

    def aux(self):
        self.response_login = my_app.test_client().post('/login', json={
            'user': "LucasERROR",
            'password': "abcdefERROR",
            'exp': 10
        })
        self.token = self.response_login.json['token']
        
    def test_error_response_login_token(self):
        self.aux()
        self.assertEqual("data incorrect or invalid", self.response_login.json['token']['msg'])

class TestNewStudent(TestCase):
    def setUp(self):
        self.n: int = n
        self.token: str = token
        self.user: str = user
        self.password: str = password
        self.aux()

    def aux(self):
        self.response_login = my_app.test_client().post('/login', json={
            'user': "Lucas",
            'password': "abcdef",
            'exp': 10
        })
        self.token = self.response_login.json['token']
        self.response_new = my_app.test_client().put(
            '/new',
            headers={
                'Authorization': "Bearer " + self.token
            },
            json={
                "name": f"Lucas Oliveira {n}",
                "color": "#03f8fc"
            }
        )

    def test_put(self):
        self.assertEqual(200, self.response_new.status_code)

    def test_content_type(self):
        self.assertIn('application/json', self.response_new.content_type)

    def test_response_new(self):
        self.assertEqual(f"table ok_LUCAS_OLIVEIRA_{n} already exists", self.response_new.json['new'][1])

"""
class TestUpdateStudent(TestCase):
    def setUp(self):
        self.n: int = n
        self.token: str = token
        self.user: str = user
        self.password: str = password
        self.data = dict(zip(
            ['bonus', 'a','b','c','d','e','f','g','h'],
            [randrange(2) for i in range(9)]
        ))
        self.aux()
        
    def aux(self):
        self.response_login = my_app.test_client().post('/login', json={
            'user': "Lucas",
            'password': "abcdef",
            'exp': 10
        })
        self.token = self.response_login.json['token']
        self.response_update = my_app.test_client().post(
            '/up',
            headers={
                'Authorization': "Bearer " + self.token
            },
            json={
                "name": f"Lucas Bispo de oliveira",
                "data": self.data
            }
        )

    def test_response_update(self):
        self.assertEqual(1, self.response_update.json)
"""

class TestDeleteStudent(TestCase):
    def setUp(self):
        self.n: int = n
        self.token: str = token
        self.user: str = user
        self.password: str = password
        self.aux()
        
    def aux(self):
        self.response_login = my_app.test_client().post('/login', json={
            'user': "Lucas",
            'password': "abcdef",
            'exp': 10
        })
        self.token = self.response_login.json['token']
        self.response_delete = my_app.test_client().delete(
            '/del',
            headers={
                'Authorization': "Bearer " + self.token
            },
            json={
                "name": f"Lucas Oliveira {n}"
            }
        )
        
    def test_response_login_token(self):
        self.assertEqual(269, len(self.response_login.json['token']))

    def test_delete(self):
        self.assertEqual(200, self.response_delete.status_code)

    def test_content_type(self):
        self.assertIn('application/json', self.response_delete.content_type)

    def test_response_delete(self):
        self.assertEqual(f"no such table: ok_LUCAS_OLIVEIRA_{n}", self.response_delete.json['deleted'])

