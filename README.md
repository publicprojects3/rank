# Banco de Dados


## *Esquemas de Tabelas Alunos:*

    Tabelas: NomeDoAluno
    Colunas: Data, Aula, Modulo, A, B, C, D, E, F, G, Score
    Descrição: 
        Data: Int =  Timestamp do momento que foi salvo
        Color: String = Cor favorita do Aluno em hexadecimal
        Aula: Int =  Número da aula (0 até 48)
        Módulo: Int = Número do modulo (1 até 3)
        A...G: Int = Pontuação sobre comportamento
        Score: Int = Total de Pontos

    Ex:
    Lucas Oliveira
    +-------------+------+---------+-------+---------+-------+---+---+---+---+---+---+---+
    | Data        | Aula |  Modulo | curso |   cor   | Score | A | B | C | D | E | F | G |
    +-------------+------+---------+-------+---------+-------+---+---+---+---+---+---+---+
    | timestamp01 |  16  |    1    | First | #ffaacc | 1000  | 1 | 1 | 0 | 1 | 1 | 1 | 0 |
    | timestamp02 |  17  |    2    | First | #ffaacc | 2000  | 1 | 1 | 0 | 1 | 1 | 1 | 0 |
    | timestamp03 |  18  |    2    | First | #ffaacc | 3400  | 1 | 1 | 1 | 1 | 1 | 1 | 0 |
    | timestamp04 |  19  |    2    | First | #ffaacc | 4000  | 1 | 1 | 0 | 1 | 1 | 1 | 0 |
    | timestamp05 |  20  |    2    | First | #ffaacc | 5600  | 1 | 1 | 0 | 1 | 1 | 1 | 0 |
    | timestamp06 |  21  |    2    | First | #ffaacc | 7500  | 1 | 1 | 0 | 1 | 1 | 1 | 0 |
    +-------------+------+---------+-------+---------+-------+---+---+---+---+---+---+---+

    **Pontuações:**
    A: Tarefa de casa
    B: Organizado
    C: Concluir dentro do tempo
    D: Programar sozinho
    E: Ajudar colega
    F: Desmontar e Guardar
    G: Desrespeito
    H: Prova

## *Esquema de Tabela Aulas:*

    Tabelas: NomeDoCurso (First, One, Tech)
    Colunas: Aula, Descricao
    Descrição:
        Aula: Int = Número da aula
        Descricao: String = Brefe descrição sobre o que se trata cada aula
        Projeto: String = Nome do objeto contruido na aula
    
    Ex:
    First

    +--------+--------------------------------+--------------+
    |  Aula  |           Descricao            |    Projeto   |
    |        | Introducao sobre o equilibrio  |              |
    |   1    | e sobre as pecas que compoe o  |   Gangorra   |
    |        | kit First                      |              |
    +--------+--------------------------------+--------------+
    |        | Aluno aprende sobre Historia   |              |
    |        | medieval, principes, reis e    |              |
    |   2    | cavaleiros, faz uma ponte      |   Castelo    |
    |        | elevadisa usando engrenagens   |              |
    +--------+--------------------------------+--------------+

# Api Docs

```
    localhost/apidocs
```
