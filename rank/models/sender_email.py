import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText as mimetext
from datetime import datetime
from os import getenv
from dotenv import load_dotenv

load_dotenv(override=True)
senha_email = getenv("PASSEMAIL")

date = datetime.now().strftime("%H:%M %d/%m/%y")

def email_log(**args):    
    if args.get('error', None)==None:
        text = f"""
-------------------{date}-------------------
                Email LOG: ENVIADO
                Nome:{args['nome']}
                Aula:{args['aula']}
                Módulo:{args['modulo']}
-------------------{date}-------------------\n\n
        """
        print(text)
        with open('rank/models/email.log', 'a') as file:
            file.write(text)
            file.close()
    else:
        text = f"""
-------------------{date}-------------------
                Email LOG[Error]:
                {args['error']}
-------------------{date}-------------------\n\n
        """
        print(text)
        with open('rank/models/email.log', 'a') as file:
            file.write(text)
            file.close()



def send(nome: str, aula: int, modulo: int) -> bool:

    mail_content = f"""
    Por favor pedir a apostila do módulo {modulo + 1} para {nome}

--------------------------{date}--------------------------------
                                Aluno: {nome}
                                Aula: {aula}
                                Módulo: {modulo}
--------------------------{date}--------------------------------
    """
    
    #The mail addresses and password section
    sender_address = 'lucasoli.work73@gmail.com'
    sender_pass = senha_email
    receiver_address = 'myrobotpprudente@gmail.com'

    #Setup the MIME
    message = MIMEMultipart()
    message['From'] = sender_address
    message['To'] = receiver_address
    message['Subject'] = f"pedir nova apostila para {nome}".upper()
    message.attach(mimetext(mail_content, 'plain'))


    #Create SMTP session for sending the mail
    session = smtplib.SMTP('smtp.gmail.com', 587) 
    session.starttls() #enable security
    session.login(sender_address, sender_pass) 
    text = message.as_string()
    try:
        session.sendmail(sender_address, receiver_address, text)
    except Exception as e:
        email_log(error=e)
        return False
    else:
        email_log(nome=nome, aula=aula, modulo=modulo)
        return True
    finally:
        session.quit()

