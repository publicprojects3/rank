if __name__=="__main__":
    from utils import *
    from sender_email import send
else:
    from rank.models.utils import *
    from rank.models.sender_email import send

from random import randrange
from numpy import array, mean



class Aluno(Util_sql):
    def __init__(self, name, color="#ff00ff"):
        self.name: str = super().valid_name(name)
        self.color: str = color
        self.__create_command: str = f"""
        CREATE TABLE {self.name} (
            data INT PRIMARY KEY, 
            aula INT, modulo INT, color CHAR(7), score INT, bonus INT,
            A INT, B INT, C INT, D INT, E INT, F INT, G INT, H INT
            )
        """
        self.initial_score: int = self.__score_mean


    @property
    def __score_mean(self) -> int:
        values = self.rank_names.values()
        if not values:
            return 0
        else:
            return int(mean([
                i[1] for i in values
            ]))


    @property
    def __exist_name(self) -> bool:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = f"SELECT * FROM {self.name}"
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(command)   
                return True, True         
            finally:
                conn.commit()


    @property
    def create_table(self) -> bool:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            try:
                cursor.execute(self.__create_command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(self.__create_command)
                return True         
            finally:
                conn.commit()
                self.__insert_table


    @property
    def __insert_table(self) -> bool:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            data = (
                super().now, 0, 0, self.color, self.initial_score,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 
            )
            command = f"INSERT INTO {self.name} VALUES {data}"
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(command)
                return True         
            finally:
                conn.commit()
                

    def __update_data(self, new_data: dict) -> list:
        data = dict(zip(
            ['timestamp', 'aula', 'modulo', 'color', 'score', 'bonus', 'a','b','c','d','e','f','g','h'],
            list(self.__select_mydata))
        )
        data.update(new_data)
        data['timestamp'] = super().now
        data['aula'] += 1
        data['score'] += self.__score(list(data.values())[6:]) + data['bonus']
        data['score'] = data['score'] if data['score'] > 0 else 0

        if data['aula'] <= 16:
            data['modulo'] = 1
        elif data['aula'] <= 32:
            data['modulo'] = 2
        else:
            data['modulo'] = 3

        
        return data


    def __score(self, lista: list) -> int:
        """
            A: Tarefa de casa
            B: Organizado
            C: Concluir dentro do tempo
            D: Programar sozinho
            E: Ajudar colega
            F: Desmontar e Guardar
            G: Advertência
            H: Prova
            bonus: bonus
        """
        x = array(lista)
        y = array([300, 200, 200, 200, 100, 200, -500, 500])
        return  int(x.dot(y))


    def update_table(self, data: dict):
        bol, value = self.__exist_name
        if not bol:
            return value
        else:
            with  sql.connect("rank/models/database/Ranking.db") as conn:
                cursor = conn.cursor()
                data = self.__update_data(data)
                command = f"INSERT INTO {self.name} VALUES {tuple(data.values())}"
                try:
                    cursor.execute(command)
                except Exception as e:
                    super().sql_log(e)
                    return False, e.args[0]
                else:
                    super().sql_log(command)
                    return True
                finally:
                    conn.commit()
                    if data['aula'] >= 28:
                        if data['aula'] < 31:
                            send(nome=self.name, aula=data['aula'], modulo=data['modulo'])
                    elif data['aula'] >= 12:
                        if data['aula'] < 16:
                            send(nome=self.name, aula=data['aula'], modulo=data['modulo'])                     


    @property
    def __select_mydata(self):
        with sql.connect("rank/models/database/Ranking.db") as conn:
            command = f"SELECT * FROM {self.name}"
            cursor = conn.cursor()
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return e.args[0]
            else:
                super().sql_log(command)
                return cursor.fetchall()[-1]
            finally:
                conn.commit()


    @property
    def delete(self):
        bol, value = self.__exist_name
        if not bol:
            return value
        n = len(super().select_delnames)

        with sql.connect("rank/models/database/Ranking.db") as conn:
            command = f"ALTER TABLE  {self.name} RENAME TO DEL_ALUNO{n}"
            cursor = conn.cursor()
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(command)
                return True
            finally:
                conn.commit()

