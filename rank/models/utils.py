from datetime import datetime
import sqlite3 as sql

class Util_sql():
    
    @property
    def now(self) -> int:
        return int(datetime.now().timestamp())
    
    def sql_log(self, command: str) -> None:
        date = datetime.now().strftime("%H:%M %d/%m/%y")
        text = f"""
        -------------------{date}-------------------
                            SQL LOG:
            {command}        
        -------------------{date}-------------------\n\n
        """
        print(text)
        with open('rank/models/database/sql.log', 'a') as file:
            file.write(text)
            file.close()
    
    def valid_name(self, name: str) -> str:
        return "ok_" + name.replace(" ", "_").upper()
        

    @property
    def select_delnames(self) -> list:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = """
            SELECT * FROM sqlite_master WHERE type='table' AND
            name LIKE 'DEL_%'
            """
            try:
                cursor.execute(command)
            except Exception as e:
                self.sql_log(e)
            else:
                self.sql_log(command)
                names = [i[1] for i in cursor.fetchall()]
                return names
            finally:
                conn.commit()

    @property
    def select_all_names(self) -> list:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = """
            SELECT * FROM sqlite_master WHERE type='table' AND
            name NOT LIKE 'First' AND
            name NOT LIKE 'One' AND
            name NOT LIKE 'Tech'
            """
            try:
                cursor.execute(command)
            except Exception as e:
                self.sql_log(e)
            else:
                self.sql_log(command)
                names = [i[1] for i in cursor.fetchall()]
                return names
            finally:
                conn.commit()

    @property
    def select_names(self) -> list:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = """
            SELECT * FROM sqlite_master WHERE type='table' AND
            name LIKE 'ok_%'
            """
            try:
                cursor.execute(command)
            except Exception as e:
                self.sql_log(e)
            else:
                self.sql_log(command)
                names = [i[1] for i in cursor.fetchall()]
                return names
            finally:
                conn.commit()


    @property
    def rank_names(self) -> dict:
        alunos = self.select_names
        rank = {}
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            try:
                for name in alunos:
                    cursor.execute(f"SELECT * FROM {name}")
                    rank[name] = list(cursor.fetchall()[-1][3:5])
                    conn.commit()
            except Exception as e:
                self.sql_log(e)
            else:
                return rank                         
