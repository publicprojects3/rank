if __name__=="__main__":
    from utils import *
else:
    from rank.models.utils import *
from hashlib import sha256


class Profs(Util_sql):
    def __init__(self) -> None:
        self.__create_command = f"""
                CREATE TABLE IF NOT EXISTS Profs (
                    user CHAR(20) PRIMARY KEY, 
                    password CHAR(255))
                """


    @property
    def create_prof(self) -> bool or tuple:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = self.__create_command
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return e.args[0]
            else:
                super().sql_log(command)   
                return True
            finally:
                conn.commit()


    def __hash_pass(self, password: str) -> str:
        return sha256(password.encode()).hexdigest()


    def insert_user(self, user: str, password: str):
        data = (user, self.__hash_pass(password))
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = f"""INSERT INTO Profs VALUES {data}"""
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(command)
                return True, True
            finally:
                conn.commit()


    def del_user(self, user):
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = f"""DELETE FROM Profs WHERE user LIKE '{user}'"""
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(command)
                return True, True
            finally:
                conn.commit()
        

    def verify_pwd(self, user: str, password: str) ->  bool:
        with sql.connect("rank/models/database/Ranking.db") as conn:
            cursor = conn.cursor()
            command = f"SELECT password FROM Profs WHERE user = '{user}'"
            try:
                cursor.execute(command)
            except Exception as e:
                super().sql_log(e)
                return False, e.args[0]
            else:
                super().sql_log(command)
                pwd = cursor.fetchall()
                if len(pwd)==0:
                    return False
                else:
                    pwd = pwd[0][0]
            finally:
                conn.commit()
        if self.__hash_pass(password)==pwd:
            return True
        else:
            return False


