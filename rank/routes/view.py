from hashlib import sha256
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from time import sleep
from random import randrange
from flasgger import Swagger
from rank.models.prof import *
from rank.models.alunos import *
from rank.routes.auth import validate
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
from flask_cors import CORS


app = Flask(__name__)
CORS(app)
api = Api(app)
swagger = Swagger(app)
app.config['JWT_SECRET_KEY'] = '123'
jwt = JWTManager(app)


class Names(Resource):
    def get(self):
        """
        This route return all students name
        ---
        responses:
          200:
            description: all names
        """
        alunos = Util_sql()
        names = alunos.select_names
        return {"names": names}


class Rank(Resource):
    def get(self):
        """
        This route return name, color and score
        ---
        responses:
          200:
            description: all data of names
        """
        Rank = Util_sql().rank_names
        rank = [
            {
                "name": name,
                "color": Rank[name][0],
                "score": Rank[name][1]
            }

            for name in Rank
        ]
        return {
            "status": "rank",
            "rank": sorted(rank, key=lambda k: k['score'], reverse=True)
        }


class Login(Resource):
    def post(self):
        """
        This is login route
        ---
        parameters:
          - in: body
            name: login
        responses:
          200:
            description: authentication route
        """
        validated = validate(request.json)

        return {
            "status": "login",
            "token": validated
        }


class NewStudent(Resource):
    @jwt_required()
    def put(self):
        try:
            name = request.json['name']
            color = request.json['color']
        except Exception as e:
            return {"Error": e.args}
        else:
            aluno = Aluno(name, color=color).create_table
            return {
                "new": aluno,
            }


class UpdateStudent(Resource):
    @jwt_required()
    def post(self):
        """
            @params data: [BONUS,A,B,C,D,E,F,G,H]
        """
        keys = ['bonus', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        values = request.json['data']
        data = dict(zip(keys, values))
        name = request.json['name']
        aluno = Aluno(name)
        response = aluno.update_table(data)

        return {
            "up": response
        }


class DeleteStudent(Resource):
    @jwt_required()
    def delete(self):
        name = request.json['name']
        aluno = Aluno(name)

        return {
            "deleted": aluno.delete
        }


class GenerateFakeStudents(Resource):
    @jwt_required()
    def post(self):
        response = request.json
        number_of_fake_student = response['number']
        name = response['name']
        aluno = Aluno(name)
        aluno.create_table
        for i in range(number_of_fake_student):
            keys = 'abcdefgh'
            values = [randrange(2) for i in range(8)]
            data = dict(zip(keys, values))
            aluno.update_table(data)
            sleep(1)
        return {
            "status": "ok"
        }


class NewProf(Resource):
    @jwt_required()
    def put(self):
        try:
            user, password = request.json.values()
        except Exception as e:
            return {"Error": e.args}
        else:
            prof = Profs()
            prof.insert_user(user, password)
            return {
                "new": True
            }


api.add_resource(Names, '/')
api.add_resource(Rank, '/rank')
api.add_resource(Login, '/login')

# metodos precisam de autenticação
api.add_resource(NewProf, '/newprof')
api.add_resource(GenerateFakeStudents, '/fake')
api.add_resource(UpdateStudent, '/up')
api.add_resource(NewStudent, '/new')
api.add_resource(DeleteStudent, '/del')
