import datetime
from rank.models.prof import Profs
from flask_jwt_extended import create_access_token


def token_generator(validated: bool, user: str):
    if validated:
        exp = datetime.timedelta(minutes=15)
        return create_access_token(identity=user, expires_delta=exp)


def validate(json: dict):
    login = Profs()
    try:
        validated = login.verify_pwd(json['user'], json['password'])
    except Exception as e:
        print(e)
        return {"Error": e.args }
    else:
        if validated:
            return token_generator(validated, json['user'])
        else:
            return {"msg": "data incorrect or invalid"}