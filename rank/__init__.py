from flask_restful import Api
from flasgger import Swagger
from flask_jwt_extended import JWTManager
from flask import Flask

app = Flask(__name__)
api = Api(app)
swagger = Swagger(app)
app.config['JWT_SECRET_KEY'] = '123'
jwt = JWTManager(app)

from rank.routes import view