fetch("http://localhost:5000/rank ")
.then(response => response.json())

/**
  * Return one element html refence 
  * @param Requests: JSON
  * @return Returns:
  * 
  *     <div class="flex-item">
  *         <p class="name" style="COLOR">NAME</p>
  *         <p class="score">SCORE</p>
  *         <img src="ADDRESS_PATENT"alt="NAME_PATENT">
  *     </div>
  */
.then(result => {
    result.rank.forEach(element => {
        document.body.appendChild(renderStudent(element))
    });
})
.catch(error => console.log(error))


function createStudent() {
    const student = document.createElement("div")
    student.classList.add("flex-item")
    return student
}

function createnName(color, text) {
    const name = document.createElement("p")
    name.classList.add("name")
    name.style.color = color
    name.textContent = text.replaceAll("_", " ").replace("ok", "")
    return name
}

function createScore(text) {
    const score = document.createElement("p")
    score.classList.add("score")
    score.textContent = text
    return score
}

function createImg(score) { 
    const img = document.createElement("img")
    img.style.width = "400px"
    img.style.height = "220px"
    img.src = "./static/Vibranium.png"
    return img
}

function renderStudent(json) {
    const div = createStudent()
    const name = createnName(json.color, json.name)
    const score = createScore(json.score)
    const img = createImg(json.score) //json.ok_LUCAS[1])

    div.appendChild(name)
    div.appendChild(score)
    div.appendChild(img)
    return div  
}
